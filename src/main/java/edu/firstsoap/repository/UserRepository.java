package edu.firstsoap.repository;

import com.firstsoap.user.Address;
import com.firstsoap.user.AddressType;
import com.firstsoap.user.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserRepository {
    private List<User> users = new ArrayList<>();

    public UserRepository() {
        User userOne = new User();
        userOne.setId(1);
        userOne.setName("Sumit Ghosh");
        userOne.setEmail("sumit.ghosh@email.com");
        Address addressOne = new Address();
        addressOne.setStreet("Garfa");
        addressOne.setCity("Kolkata");
        addressOne.setState("WB");
        addressOne.setCountry("India");
        addressOne.setZip(700030);
        addressOne.setAddressType(AddressType.COMMUNICATION);
        userOne.setAddress(addressOne);

        User userTwo = new User();
        userTwo.setId(2);
        userTwo.setName("Loku Poddar");
        userTwo.setEmail("debabrata.poddar@email.com");
        Address addressTwo = new Address();
        addressTwo.setStreet("Birati");
        addressTwo.setCity("Kolkata");
        addressTwo.setState("WB");
        addressTwo.setCountry("India");
        addressTwo.setZip(700130);
        addressTwo.setAddressType(AddressType.COMMUNICATION);
        userTwo.setAddress(addressTwo);

        User userThree = new User();
        userThree.setId(3);
        userThree.setName("Souvik Sanyal");
        userThree.setEmail("souvik.sanyal@email.com");
        Address addressThree = new Address();
        addressThree.setStreet("Kalighat");
        addressThree.setCity("Kolkata");
        addressThree.setState("WB");
        addressThree.setCountry("India");
        addressThree.setZip(700150);
        addressThree.setAddressType(AddressType.COMMUNICATION);
        userThree.setAddress(addressThree);

        User userFour = new User();
        userFour.setId(4);
        userFour.setName("Liton Sarkar");
        userFour.setEmail("liton.sarkar@email.com");
        Address addressFour = new Address();
        addressFour.setStreet("Sukanta Nagar");
        addressFour.setCity("Kolkata");
        addressFour.setState("WB");
        addressFour.setCountry("India");
        addressFour.setZip(700098);
        addressFour.setAddressType(AddressType.COMMUNICATION);
        userFour.setAddress(addressFour);

        User userFive = new User();
        userFive.setId(5);
        userFive.setName("Rushikesh Mukund Fanse");
        userFive.setEmail("rushikesh.fanse@email.com");
        Address addressFive = new Address();
        addressFive.setStreet("Nasik");
        addressFive.setCity("Mumbai");
        addressFive.setState("MH");
        addressFive.setCountry("India");
        addressFive.setZip(400091);
        addressFive.setAddressType(AddressType.COMMUNICATION);
        userFive.setAddress(addressFive);

        users.add(userOne);
        users.add(userTwo);
        users.add(userThree);
        users.add(userFour);
        users.add(userFive);
    }

    public List<User> getUsers(String name) {
        List<User> userList = new ArrayList<>();
        for (User user : users) {
            if (user.getName().toLowerCase().contains(name.toLowerCase())) {
                userList.add(user);
            }
        }
        return userList;
    }
}
