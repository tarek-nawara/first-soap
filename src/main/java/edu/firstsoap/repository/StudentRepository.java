package edu.firstsoap.repository;

import com.firstsoap.student.StudentDetails;
import org.springframework.stereotype.Component;

@Component
public class StudentRepository {

    public StudentDetails getStudentDetails(int id) {
        StudentDetails studentDetails = new StudentDetails();
        studentDetails.setId(id);
        studentDetails.setName("test");
        studentDetails.setPassportNumber("022020");
        return studentDetails;
    }
}
