package edu.firstsoap.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();
        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(servlet, "/ws/*");
    }

    @Bean(name = "users")
    public DefaultWsdl11Definition usersWsdl11Definition(@Qualifier("usersSchema") XsdSchema userSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("UserPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("https://firstsopa.com/UserService");
        wsdl11Definition.setSchema(userSchema);
        return wsdl11Definition;
    }


    @Bean(name = "students")
    public DefaultWsdl11Definition studentsWsdl11Definition(@Qualifier("studentsSchema") XsdSchema studentSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("StudentPort");
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace("https://firstsopa.com/StudentService");
        wsdl11Definition.setSchema(studentSchema);
        return wsdl11Definition;
    }


    @Bean
    @Qualifier("usersSchema")
    public XsdSchema usersSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/user.xsd"));
    }

    @Bean
    @Qualifier("studentsSchema")
    public XsdSchema studentSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/student.xsd"));
    }
}
