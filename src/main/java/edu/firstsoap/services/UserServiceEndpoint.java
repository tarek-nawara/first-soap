package edu.firstsoap.services;

import com.firstsoap.user.GetUserDetailsRequest;
import com.firstsoap.user.GetUserDetailsResponse;
import edu.firstsoap.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class UserServiceEndpoint {
    private final String NAMESPACE = "https://firstsoap.com/UserService";

    private final UserRepository userRepository;

    @Autowired
    public UserServiceEndpoint(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PayloadRoot(localPart = "GetUserDetailsRequest", namespace = NAMESPACE)
    @ResponsePayload
    public GetUserDetailsResponse getUser(@RequestPayload final GetUserDetailsRequest request) {
        GetUserDetailsResponse response = new GetUserDetailsResponse();
        response.getUsers().addAll(userRepository.getUsers(request.getName()));
        return response;
    }

}
