package edu.firstsoap.services;

import com.firstsoap.student.GetStudentDetailsRequest;
import com.firstsoap.student.GetStudentDetailsResponse;
import edu.firstsoap.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class StudentServiceEndpoint {
    private final String NAMESPACE = "https://firstsoap.com/StudentService";

    private final StudentRepository studentRepository;

    @Autowired
    public StudentServiceEndpoint(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @PayloadRoot(localPart = "getStudentDetailsRequest", namespace = NAMESPACE)
    @ResponsePayload
    public GetStudentDetailsResponse getUser(@RequestPayload final GetStudentDetailsRequest request) {
        GetStudentDetailsResponse response = new GetStudentDetailsResponse();
        response.setStudentDetails(studentRepository.getStudentDetails(request.getId()));
        return response;
    }

}
