package edu.firstsoap.client;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.annotation.PostConstruct;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.util.Objects;

@Service
public class AnotherClient extends WebServiceGatewaySupport {
    private static final String URL_PERIODIC = "http://localhost:9999/ws/students.wsdl";

    @PostConstruct
    public void init() throws SOAPException {
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        SaajSoapMessageFactory newSoapMessageFactory = new SaajSoapMessageFactory(messageFactory);
        super.setMessageFactory(newSoapMessageFactory);
        super.setDefaultUri(URL_PERIODIC);
    }

    public void anotherSend() throws IOException {
        String request = IOUtils.toString(Objects.requireNonNull(
                getClass().getClassLoader().getResourceAsStream("request.xml")), Charset.defaultCharset());
        System.out.println(request);
        StreamSource source = new StreamSource(new StringReader(request));
        StreamResult result = new StreamResult(System.out);
        getWebServiceTemplate().sendSourceAndReceiveToResult(URL_PERIODIC, source,
                new SoapActionCallback("http://localhost:9999/ws/getStudentDetailsRequest"), result);
    }
}
